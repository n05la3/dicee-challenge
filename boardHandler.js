
/**
 *
 * @param from min value for randomness
 * @param to max value for randomness
 * @returns Integer
 */
function randomInt(from, to) {
    return Math.floor(Math.random() * to) + from;
}

/**
 * Initializes and plays the game
 * @return void
 */
function initGame() {
    var diceImg = document.querySelectorAll("img");

    //ensure elements exists
    if(typeof diceImg !== 'undefined') {

        if(diceImg.length > 1) {
            //generate random numbers
            var fstRand = randomInt(1, 6);
            var sndRand = randomInt(1, 6);

            //place the dice on board
            diceImg[0].setAttribute('src', `images/dice${fstRand}.png`);
            diceImg[1].setAttribute('src', `images/dice${sndRand}.png`);

            //proclaim results
            if(fstRand > sndRand) {
                document.querySelector('h1').innerHTML = 'Player 1 wins';
            } else if(sndRand > fstRand) {
                document.querySelector('h1').innerHTML = 'Player 2 wins';
            } else {
                document.querySelector('h1').innerHTML = 'It\'s a draw';
            }

            return;
        }
    }
    //generic error message
    alert("Some elements seem to be missing from DOM");
}